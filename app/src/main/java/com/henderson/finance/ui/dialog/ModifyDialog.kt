package com.henderson.finance.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import com.henderson.finance.R
import com.henderson.finance.databinding.DialogModifyBinding
import com.henderson.finance.model.Investment
import com.henderson.finance.utils.db.DataBase


class ModifyDialog(internal val context: Context, val investment: Investment) : Dialog(context, R.style.CornerRoundDialog) {
    private var binding: DialogModifyBinding? = null

    var modifyListener: ModifyListener? = null

    var category = MutableLiveData("")
    var price = MutableLiveData("")
    var fromDate = MutableLiveData("")
    var toDate = MutableLiveData("")
    var collection = MutableLiveData("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initDialog()
        setData()
    }

    private fun initDialog() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_modify, null, false)
        binding!!.viewModel = this
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(binding!!.root)
    }

    private fun setData() {
        category.value = investment.category
        price.value = investment.price.toString()
        fromDate.value = investment.fromDate
        toDate.value = investment.toDate
        collection.value = investment.collection.toString()
    }

    fun clickModify(view: View) {
        investment.category = category.value
        investment.price = price.value!!.toInt()
        investment.fromDate = fromDate.value
        investment.toDate = toDate.value
        investment.collection = collection.value!!.toInt()

        Thread {
            DataBase.getInstance(context)?.investmentDao()?.updateInvestment(investment)
            modifyListener?.onSuccess()
            dismiss()
        }.start()
    }

    fun clickDelete(view: View) {
        Thread {
            DataBase.getInstance(context)?.investmentDao()?.deleteInvestment(investment)
            modifyListener?.onSuccess()
            dismiss()
        }.start()
    }

    fun clickCancel(view: View) {
        dismiss()
    }

    interface ModifyListener {
        fun onSuccess()
    }
}