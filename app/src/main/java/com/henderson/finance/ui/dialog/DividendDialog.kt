package com.henderson.finance.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import com.henderson.finance.R
import com.henderson.finance.databinding.DialogAddDividendBinding
import com.henderson.finance.model.Dividend

class DividendDialog(internal val context: Context) : Dialog(context, R.style.CornerRoundDialog) {
    private var binding: DialogAddDividendBinding? = null
    var dividendDialogListener: AddDividendDialogListener? = null

    var dividendData: Dividend? = null

    var category = MutableLiveData("")
    var dividend = MutableLiveData("")
    var date = MutableLiveData("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initDialog()
        setData()
    }

    private fun initDialog() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_add_dividend, null, false)
        binding!!.viewModel = this
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(binding!!.root)
    }

    private fun setData() {
        if (dividendData == null) {
            binding?.modifyButton?.visibility = View.GONE
            binding?.deleteButton?.visibility = View.GONE
            binding?.addButton?.visibility = View.VISIBLE
        } else {
            binding?.modifyButton?.visibility = View.VISIBLE
            binding?.deleteButton?.visibility = View.VISIBLE
            binding?.addButton?.visibility = View.INVISIBLE

            category.value = dividendData!!.category
            dividend.value = dividendData!!.dividend.toString()
            date.value = dividendData!!.date
        }
    }

    fun clickModify(view: View) {
        if (TextUtils.isEmpty(category.value!!)) {
            Toast.makeText(context, R.string.toast_empty, Toast.LENGTH_SHORT).show()
            return
        } else if (TextUtils.isEmpty(dividend.value!!)) {
            Toast.makeText(context, R.string.toast_empty, Toast.LENGTH_SHORT).show()
            return
        } else {
            dividendData!!.category = category.value
            dividendData!!.dividend = dividend.value!!.toInt()
            dividendData!!.date = date.value

            dismiss()
            dividendDialogListener?.updateDividend(dividendData!!)
        }
    }

    fun clickDelete(view: View) {
        dismiss()
        dividendDialogListener?.deleteDividend(dividendData!!)
    }

    fun clickAdd(view: View) {
        if (TextUtils.isEmpty(category.value!!)) {
            Toast.makeText(context, R.string.toast_empty, Toast.LENGTH_SHORT).show()
            return
        } else if (TextUtils.isEmpty(dividend.value!!)) {
            Toast.makeText(context, R.string.toast_empty, Toast.LENGTH_SHORT).show()
            return
        } else {
            Dividend().also {
                it.category = category.value
                it.dividend = dividend.value!!.toInt()
                it.date = date.value
            }.run {
                dismiss()
                dividendDialogListener?.addDividend(this)
            }
        }
    }

    fun clickCancel(view: View) {
        dismiss()
    }

    interface AddDividendDialogListener {
        fun updateDividend(dividend: Dividend)
        fun deleteDividend(dividend: Dividend)
        fun addDividend(dividend: Dividend)
    }
}