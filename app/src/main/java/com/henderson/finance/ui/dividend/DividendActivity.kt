package com.henderson.finance.ui.dividend

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.gms.ads.*
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.henderson.finance.BuildConfig
import com.henderson.finance.R
import com.henderson.finance.adapter.DividendAdapter
import com.henderson.finance.databinding.ActivityDividendBinding
import com.henderson.finance.model.Dividend
import com.henderson.finance.ui.base.BaseActivity
import com.henderson.finance.ui.dialog.DividendDialog
import com.henderson.finance.utils.Constant
import com.henderson.finance.utils.PreferenceManager


class DividendActivity : BaseActivity<ActivityDividendBinding>(), DividendListener, DividendDialog.AddDividendDialogListener, DividendAdapter.DividendAdapterListener {
    override val layoutResourceID: Int
        get() = R.layout.activity_dividend

    private lateinit var viewModel: DividendViewModel

    private var adapter: DividendAdapter? = null

    private lateinit var adView: AdView
    private var rewardedAd: RewardedAd? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initBinding()
        initToolBar()
        initAd()
    }

    override fun onResume() {
        super.onResume()

        viewModel.initData()
    }

    private fun initView() {
        viewModel = ViewModelProviders.of(this).get(DividendViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.listener = this

        //Init RecyclerView
        adapter = DividendAdapter(this)
        adapter!!.listener = this
        binding.dividendList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        binding.dividendList.adapter = adapter
    }

    private fun initBinding() {
        viewModel.dividendList.observe(this) {
            adapter!!.setDividendList(viewModel.dividendList.value!!)
            toast(this, R.string.toast_complete_modify)
        }

        viewModel.insertingResult.observe(this) {
            if (it.toInt() != 0) {
                viewModel.getDividendList()
                toast(this, R.string.toast_done_add)
            }
        }
    }

    private fun initToolBar() {
        setSupportActionBar(binding.toolbar)

        val actionBar = supportActionBar
        actionBar?.setDisplayShowTitleEnabled(false)
        actionBar?.setDisplayShowHomeEnabled(true)
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initAd() {
        MobileAds.initialize(this) {}

        //Set Banner ad
        adView = AdView(this)
        adView.adSize = AdSize.BANNER
        if (BuildConfig.DEBUG)
            adView.adUnitId = resources.getString(R.string.banner_test_ad_unit_id)
        else
            adView.adUnitId = resources.getString(R.string.banner_ad_unit_id)

        adView.loadAd(AdRequest.Builder().build())
        binding.advertisement.addView(adView)

        //Set Rewarded ad
        setRewardedAdd()
    }

    private fun setRewardedAdd() {
        val unitId =
            if (BuildConfig.DEBUG) resources.getString(R.string.reward_test_ad_unit_id)
            else resources.getString(R.string.reward_ad_unit_id)

        RewardedAd.load(this, unitId, AdRequest.Builder().build(), object : RewardedAdLoadCallback() {
            override fun onAdFailedToLoad(adError: LoadAdError) {
                rewardedAd = null
            }

            override fun onAdLoaded(rewardedAd: RewardedAd) {
                this@DividendActivity.rewardedAd = rewardedAd
            }
        })

        rewardedAd?.fullScreenContentCallback = object : FullScreenContentCallback() {
            override fun onAdShowedFullScreenContent() {}

            override fun onAdFailedToShowFullScreenContent(adError: AdError?) {}

            override fun onAdDismissedFullScreenContent() { rewardedAd = null }
        }
    }

    /* Invest Listener */
    override fun showRewardedVideoAd() {
        if (rewardedAd != null) {
            rewardedAd?.show(this) {
                setRewardedAdd()
                PreferenceManager(this).addAvailableCount()
                viewModel.availableCount.value = PreferenceManager.getInt(this, Constant.AVAILABLE_COUNT)
                toast(this, R.string.toast_rewarded)
            }
        } else {
            toast(this, R.string.toast_ad_not_ready)
        }
    }

    override fun showAddDividendDialog() {
        DividendDialog(this).also {
            it.dividendDialogListener = this
        }.run {
            show()
        }
    }
    /* */

    /* DividendDialog Listener */
    override fun updateDividend(dividend: Dividend) {
        viewModel.updateDividend(dividend)
        viewModel.initData()
    }

    override fun deleteDividend(dividend: Dividend) {
        viewModel.deleteDividend(dividend)
        viewModel.initData()
    }

    override fun addDividend(dividend: Dividend) {
        viewModel.addDividend(dividend)
        viewModel.initData()
    }
    /* */

    /* Dividend Adapter Listener */
    override fun clicked(position: Int) {
        DividendDialog(this).also {
            it.dividendDialogListener = this
            it.dividendData = viewModel.dividendList.value?.get(position)
        }.run {
            show()
        }
    }
    /* */
}