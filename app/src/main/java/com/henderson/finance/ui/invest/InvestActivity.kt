package com.henderson.finance.ui.invest

import android.animation.Animator
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageButton
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.gms.ads.*
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.henderson.finance.BuildConfig
import com.henderson.finance.R
import com.henderson.finance.adapter.InvestmentAdapter
import com.henderson.finance.adapter.TipAdapter
import com.henderson.finance.databinding.ActivityInvestBinding
import com.henderson.finance.model.Investment
import com.henderson.finance.ui.base.NavigationActivity
import com.henderson.finance.ui.dialog.AddInvestmentDialog
import com.henderson.finance.ui.dialog.ModifyDialog
import com.henderson.finance.utils.Constant
import com.henderson.finance.utils.PreferenceManager

class InvestActivity : NavigationActivity<ActivityInvestBinding>(), InvestListener, InvestmentAdapter.InvestmentAdapterListener, AddInvestmentDialog.AddInvestmentDialogListener, ModifyDialog.ModifyListener {
    override val layoutResourceID: Int
        get() = R.layout.activity_invest

    override val drawer: DrawerLayout
        get() = binding.drawer
    override val hamburger: ImageButton
        get() = binding.toolbarHamburger

    private lateinit var viewModel: InvestViewModel

    private var investmentAdapter: InvestmentAdapter? = null

    //Local Variable, Constants for ViewPagerIndicator
    private var tipAdapter: TipAdapter? = null
    private val bannerInterval: Long = 4000
    private val handler = Handler()
    private val runnableForBanner = object : Runnable {
        override fun run() {
            goToNextBanner()
            handler.postDelayed(this, bannerInterval)
        }
    }

    private lateinit var adView: AdView
    private var rewardedAd: RewardedAd? = null

    //Local Constants for Animation
    private val duration = 500L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initBinding()
        initToolBar()
        initAd()
    }

    override fun onResume() {
        super.onResume()

        viewModel.initData()
        viewModel.setTotalData()

        handler.postDelayed(runnableForBanner, bannerInterval)
    }

    override fun onPause() {
        super.onPause()

        handler.removeCallbacksAndMessages(null)
    }
    
    private fun initView() {
        viewModel = ViewModelProviders.of(this).get(InvestViewModel::class.java)
        viewModel.listener = this
        binding.viewmodel = viewModel

        //Init RecyclerView
        investmentAdapter = InvestmentAdapter(this)
        investmentAdapter!!.listener = this
        binding.investList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        binding.investList.adapter = investmentAdapter

        //Init ViewPager
        tipAdapter = TipAdapter()
        binding.investTipBanner.adapter = tipAdapter
    }

    private fun initBinding() {
        viewModel.investmentList.observe(this) {
            investmentAdapter!!.setInvestmentList(viewModel.investmentList.value!!)
            viewModel.setTotalData()
            toast(this, R.string.toast_complete_modify)
        }

        viewModel.insertingResult.observe(this) {
            if (it.toInt() != 0) {
                viewModel.getInvestmentList()
                toast(this, R.string.toast_done_add)
            }
        }

        viewModel.isTotalViewVisible.observe(this, Observer { isVisible ->
            if (isVisible) {
                binding.totalButton.apply {
                    setImageDrawable(getDrawable(R.drawable.arrow_down))
                    (drawable as AnimationDrawable).start()
                }
                binding.totalLayout.let {
                    it.animate()
                            .translationY(0f)
                            .setDuration(duration)
                            .setListener(object : Animator.AnimatorListener {
                                override fun onAnimationStart(animation: Animator?) {
                                    it.visibility = View.VISIBLE
                                }

                                override fun onAnimationEnd(animation: Animator?) {}
                                override fun onAnimationCancel(animation: Animator?) {}
                                override fun onAnimationRepeat(animation: Animator?) {}
                            })
                }
            } else {
                binding.totalButton.apply {
                    setImageDrawable(getDrawable(R.drawable.arrow_up))
                    (drawable as AnimationDrawable).start()
                }
                binding.totalLayout.let {
                    it.animate()
                            .translationY(it.height.toFloat())
                            .setDuration(duration)
                            .setListener(object : Animator.AnimatorListener {
                                override fun onAnimationStart(animation: Animator?) {}
                                override fun onAnimationEnd(animation: Animator?) {
                                    it.visibility = View.GONE
                                }

                                override fun onAnimationCancel(animation: Animator?) {}
                                override fun onAnimationRepeat(animation: Animator?) {}
                            })
                }
            }
        })
    }

    private fun initToolBar() {
        setSupportActionBar(binding.toolbar)

        val actionBar = supportActionBar
        actionBar?.setDisplayShowTitleEnabled(false)
        actionBar?.setDisplayShowHomeEnabled(false)
        actionBar?.setDisplayHomeAsUpEnabled(false)
    }

    private fun initAd() {
        MobileAds.initialize(this) {}

        //Set Banner ad
        adView = AdView(this)
        adView.adSize = AdSize.BANNER
        if (BuildConfig.DEBUG)
            adView.adUnitId = resources.getString(R.string.banner_test_ad_unit_id)
        else
            adView.adUnitId = resources.getString(R.string.banner_ad_unit_id)

        adView.loadAd(AdRequest.Builder().build())
        binding.advertisement.addView(adView)

        //Set Rewarded ad
        setRewardedAdd()
    }

    private fun setRewardedAdd() {
        val unitId =
            if (BuildConfig.DEBUG) resources.getString(R.string.reward_test_ad_unit_id)
            else resources.getString(R.string.reward_ad_unit_id)

        RewardedAd.load(this, unitId, AdRequest.Builder().build(), object : RewardedAdLoadCallback() {
            override fun onAdFailedToLoad(adError: LoadAdError) {
                rewardedAd = null
            }

            override fun onAdLoaded(rewardedAd: RewardedAd) {
                this@InvestActivity.rewardedAd = rewardedAd
            }
        })

        rewardedAd?.fullScreenContentCallback = object : FullScreenContentCallback() {
            override fun onAdShowedFullScreenContent() {}

            override fun onAdFailedToShowFullScreenContent(adError: AdError?) {}

            override fun onAdDismissedFullScreenContent() { rewardedAd = null }
        }
    }

    private fun goToNextBanner() {
        var currentCount = binding.investTipBanner.currentItem
        if (currentCount < tipAdapter!!.bannerCount - 1) {
            currentCount++
        } else {
            currentCount = 0
        }
        binding.investTipBanner.currentItem = currentCount
    }


    //Invest Listener
    override fun showRewardedVideoAd() {
        if (rewardedAd != null) {
            rewardedAd?.show(this) {
                setRewardedAdd()
                PreferenceManager(this).addAvailableCount()
                viewModel.availableCount.value = PreferenceManager.getInt(this, Constant.AVAILABLE_COUNT)
                toast(this, R.string.toast_rewarded)
            }
        } else {
            toast(this, R.string.toast_ad_not_ready)
        }
    }

    override fun showAddInvestmentDialog() {
        AddInvestmentDialog(this).also {
            it.addInvestmentDialogListener = this
        }.run {
            show()
        }
    }

    //Investment Adapter Listener
    override fun clicked(position: Int) {
        ModifyDialog(this, viewModel.investmentList.value!![position]).also {
            it.modifyListener = this
        }.run {
            show()
        }
    }

    /* AddInvestmentBottomSheet Listener */
    override fun inputOver(investment: Investment) {
        viewModel.addInvestment(investment)
    }

    /* ModifyDialog Listener */
    override fun onSuccess() {
        viewModel.getInvestmentList()
    }
}
