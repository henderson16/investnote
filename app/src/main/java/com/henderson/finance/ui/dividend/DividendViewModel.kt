package com.henderson.finance.ui.dividend

import android.app.Application
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.henderson.finance.R
import com.henderson.finance.model.Dividend
import com.henderson.finance.ui.base.BaseViewModel
import com.henderson.finance.utils.Constant
import com.henderson.finance.utils.PreferenceManager
import com.henderson.finance.utils.db.DataBase

class DividendViewModel(application: Application) : BaseViewModel(application) {
    var listener: DividendListener? = null

    //Local Variable
    var availableCount = MutableLiveData<Int>()
    var dividendList = MutableLiveData<ArrayList<Dividend>>()
    var insertingResult = MutableLiveData<Long>()

    fun initData() {
        availableCount.value = PreferenceManager.getInt(getApplication(), Constant.AVAILABLE_COUNT)
        getDividendList()
    }

    fun getDividendList() {
        Thread {
            dividendList.postValue(
                DataBase.getInstance(getApplication())?.dividendDao()
                    ?.getDividendList() as ArrayList<Dividend>
            )
        }.start()
    }

    fun showAddDividendDialog(view: View) {
        val coin = PreferenceManager.getInt(getApplication(), Constant.AVAILABLE_COUNT)
        if (coin > 0) {
            listener?.showAddDividendDialog()
        } else {
            toast(getApplication(), R.string.toast_no_coin)
        }
    }

    fun showRewardedVideoAd(view: View) {
        listener?.showRewardedVideoAd()
    }

    fun updateDividend(dividend: Dividend) {
        Thread {
            DataBase.getInstance(getApplication())?.dividendDao()?.updateDividend(dividend)
        }.start()
    }

    fun deleteDividend(dividend: Dividend) {
        Thread {
            DataBase.getInstance(getApplication())?.dividendDao()?.deleteDividend(dividend)
        }.start()
    }

    fun addDividend(dividend: Dividend) {
        Thread {
            val result =
                DataBase.getInstance(getApplication())?.dividendDao()?.insertDividend(dividend)
            insertingResult.postValue(result)
            PreferenceManager(getApplication()).removeAvailableCount()
            availableCount.postValue(
                PreferenceManager.getInt(
                    getApplication(), Constant.AVAILABLE_COUNT
                )
            )
        }.start()
    }
}

interface DividendListener {
    fun showRewardedVideoAd()
    fun showAddDividendDialog()
}