package com.henderson.finance.ui.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import com.henderson.finance.R
import com.henderson.finance.databinding.DialogAddInvestmentBinding
import com.henderson.finance.model.Investment

class AddInvestmentDialog(internal val context: Context) : Dialog(context, R.style.CornerRoundDialog) {
    private var binding: DialogAddInvestmentBinding? = null
    var addInvestmentDialogListener: AddInvestmentDialogListener? = null

    var category = MutableLiveData<String>("")
    var price = MutableLiveData<String>("")
    var collection = MutableLiveData<String>("")
    var fromDate = MutableLiveData<String>("")
    var toDate = MutableLiveData<String>("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initDialog()
    }

    private fun initDialog() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_add_investment, null, false)
        binding!!.viewModel = this
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(binding!!.root)
    }

    fun clickAdd(view: View) {
        if (TextUtils.isEmpty(category.value!!)) {
            Toast.makeText(context, R.string.toast_investment_empty, Toast.LENGTH_SHORT).show()
            return
        } else if (TextUtils.isEmpty(price.value!!)) {
            Toast.makeText(context, R.string.toast_investment_empty, Toast.LENGTH_SHORT).show()
            return
        } else {
            Investment().also {
                it.category = category.value
                it.price = price.value!!.toInt()
                if (!TextUtils.isEmpty(collection.value)) {
                    it.collection = collection.value!!.toInt()
                }
                it.fromDate = fromDate.value
                it.toDate = toDate.value
            }.run {
                addInvestmentDialogListener?.inputOver(this)
                dismiss()
            }
        }
    }

    fun clickCancel(view: View) {
        dismiss()
    }

    interface AddInvestmentDialogListener {
        fun inputOver(investment: Investment)
    }
}