package com.henderson.finance.ui.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

import com.henderson.finance.BuildConfig
import com.henderson.finance.ui.dialog.LoadingDialog

abstract class BaseActivity<T: ViewDataBinding> : AppCompatActivity() {
    lateinit var binding: T
    abstract val layoutResourceID: Int

    protected var loadingDialog: LoadingDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        log("onCreate")
        initBinding()
    }

    override fun onStart() {
        super.onStart()
        log("onStart")
    }

    override fun onResume() {
        super.onResume()
        log("onResume")
    }

    override fun onStop() {
        super.onStop()
        log("onStop")
    }

    override fun onPause() {
        super.onPause()
        log("onPause")
    }

    override fun onDestroy() {
        super.onDestroy()
        log("onDestroy")
    }

    private fun initBinding() {
        binding = DataBindingUtil.setContentView(this, layoutResourceID)
        binding.lifecycleOwner = this
    }

    protected fun showProgressBar() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialog(this)
        }
        loadingDialog!!.show()
    }

    protected fun hideProgressBar() {
        if (loadingDialog!!.isShowing) {
            loadingDialog!!.dismiss()
        }
    }

    protected fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    /*	private long interval = 1000;
		private long previous = 0;
		private long current = 0; */
    override fun onBackPressed() {
        super.onBackPressed()
        /*
		current = System.currentTimeMillis();
		if(current - previous < interval) {
			super.onBackPressed();
		}
		else {
			previous = current;
			toast(R.string.back_button);
		}
		*/
    }

    fun toast(context: Context, msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }

    fun toast(context: Context, msg: Int) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }


    //Toolbar Item Event
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }


    //Example : log("getLocalIpAddress", "ip : " + ip);
    protected fun log(methodName: String, msg: String) {
        if (BuildConfig.DEBUG) Log.d("!!" + javaClass.simpleName, "$methodName $msg")
    }

    protected fun log(methodName: String) {
        this.log(methodName, "")
    }
}
