package com.henderson.finance.ui.dialog

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.henderson.finance.R
import com.henderson.finance.databinding.DialogAddInvestmentBinding
import com.henderson.finance.model.Investment

//class AddInvestmentBottomSheet(internal val context: Context) : BottomSheetDialogFragment() {
//    private var binding: DialogAddInvestmentBinding? = null
//    var addInvestmentDialogListener: AddInvestmentDialogListener? = null
//
//    var category = MutableLiveData<String>("")
//    var price = MutableLiveData<String>("")
//    var collection = MutableLiveData<String>("")
//    var fromDate = MutableLiveData<String>("")
//    var toDate = MutableLiveData<String>("")
//
//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        initView()
//        return binding!!.root
//    }
//
//    private fun initView() {
//        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_add_investment, null, false)
//        binding!!.viewModel = this
//        binding!!.lifecycleOwner = this
//    }
//
//    fun clickAdd(view: View) {
//        if (TextUtils.isEmpty(category.value!!)) {
//            Toast.makeText(context, R.string.toast_empty, Toast.LENGTH_SHORT).show()
//            return
//        }
//        else if (TextUtils.isEmpty(price.value!!)) {
//            Toast.makeText(context, R.string.toast_empty, Toast.LENGTH_SHORT).show()
//            return
//        } else {
//            Investment().also {
//                it.category = category.value
//                it.price = price.value!!.toInt()
//                if (!TextUtils.isEmpty(collection.value)) { it.collection = collection.value!!.toInt() }
//                it.fromDate = fromDate.value
//                it.toDate = toDate.value
//            }.run {
//                addInvestmentDialogListener?.inputOver(this)
//                dismiss()
//            }
//        }
//    }
//
//    override fun getTheme(): Int {
//        return R.style.BottomSheetDialogWithBackground
//    }
//
//    interface AddInvestmentDialogListener {
//        fun inputOver(investment: Investment)
//    }
//}