package com.henderson.finance.ui.chart

import android.os.Bundle

import androidx.appcompat.app.ActionBar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders

import com.henderson.finance.ui.base.BaseActivity
import com.henderson.finance.R
import com.henderson.finance.databinding.ActivityChartBinding
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.initialization.InitializationStatus
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener
import com.henderson.finance.BuildConfig


class ChartActivity : BaseActivity<ActivityChartBinding>() {
    override val layoutResourceID: Int
        get() = R.layout.activity_chart

    private lateinit var viewModel: ChartViewModel

    private lateinit var adView: AdView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initView()
        initToolBar()
        initAd()
    }

    private fun initView() {
        viewModel = ViewModelProviders.of(this).get(ChartViewModel::class.java)
    }


    private fun initToolBar() {
        log("initToolBar")
        setSupportActionBar(binding.chartToolbar)

        val actionBar = supportActionBar
        actionBar!!.setDisplayShowTitleEnabled(false)
        actionBar.setDisplayShowHomeEnabled(false)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    private fun initAd() {
        MobileAds.initialize(this)
        val adRequest = AdRequest.Builder().build()
        adView = AdView(this)
        adView.adSize = AdSize.BANNER
        if (BuildConfig.DEBUG)
            adView.adUnitId = resources.getString(R.string.banner_test_ad_unit_id)
        else
            adView.adUnitId = resources.getString(R.string.banner_ad_unit_id)

        adView.loadAd(adRequest)
        binding.chartAd.addView(adView)
    }
}
