package com.henderson.finance.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.henderson.finance.R
import com.henderson.finance.model.Investment
import com.henderson.finance.utils.BaseUtil
import java.util.*

class InvestmentAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var investmentList: List<Investment> = ArrayList()

    var listener: InvestmentAdapterListener? = null

    inner class InvestmentCell internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var layout: ConstraintLayout
        internal var category: TextView
        internal var price: TextView
        internal var collection: TextView
        internal var fromDate: TextView
        internal var toDate: TextView
        internal var earningPrice: TextView
        internal var earningRate: TextView

        init {
            layout = itemView.findViewById(R.id.item_invest_parent)
            category = itemView.findViewById(R.id.item_invest_category)
            price = itemView.findViewById(R.id.item_invest_price)
            collection = itemView.findViewById(R.id.item_invest_collection)
            fromDate = itemView.findViewById(R.id.item_invest_date_from)
            toDate = itemView.findViewById(R.id.item_invest_date_to)
            earningPrice = itemView.findViewById(R.id.item_invest_earning_price)
            earningRate = itemView.findViewById(R.id.item_invest_earning_rate)

            layout.setOnClickListener {
                listener!!.clicked(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.item_investment, parent, false)
        return InvestmentCell(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val cell = holder as InvestmentCell
        val item = investmentList[position]

        cell.category.text = item.category
        cell.price.text = BaseUtil.addComma(item.price)
        cell.collection.text = BaseUtil.addComma(item.collection)

        val earningPrice = item.collection - item.price
        var earningRate = if (item.price == 0 || earningPrice == 0) { 0f } else { (earningPrice / item.price.toFloat()) * 100 }
        cell.earningPrice.text = (if (earningPrice > 0) "+" else "") + BaseUtil.addComma(earningPrice)
        cell.earningRate.text = (if (earningRate > 0) "+" else "") + context.getString(R.string.percent, earningRate)

        setTextWithColor(cell.earningPrice, earningPrice)
        setTextWithColor(cell.earningRate, earningRate.toInt())

        cell.fromDate.text = "" + item.fromDate
        if (item.toDate == null || item.toDate!!.isEmpty()) {
            cell.toDate.text = null
        } else {
            cell.toDate.text = "" + item.toDate
        }
    }


    private fun setTextWithColor(textView: TextView, number: Int) {
        if (number > 0) {
            textView.setTextColor(context.getColor(R.color.red))
        } else if (number == 0) {
            textView.setTextColor(context.getColor(R.color.black))
        } else {
            textView.setTextColor(context.getColor(R.color.dodger_blue))
        }
    }

    override fun getItemCount(): Int {
        return this.investmentList.size
    }

    fun setInvestmentList(items: ArrayList<Investment>) {
        this.investmentList = items
        notifyDataSetChanged()
    }

    interface InvestmentAdapterListener {
        fun clicked(position: Int)
    }
}