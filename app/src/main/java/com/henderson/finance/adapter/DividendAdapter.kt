package com.henderson.finance.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.henderson.finance.R
import com.henderson.finance.model.Dividend
import com.henderson.finance.utils.BaseUtil
import java.util.*

class DividendAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var dividendList: List<Dividend> = ArrayList()

    var listener: DividendAdapterListener? = null

    inner class DividendCell internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var category: TextView
        internal var dividend: TextView
        internal var date: TextView

        init {
            category = itemView.findViewById(R.id.item_dividend_category)
            dividend = itemView.findViewById(R.id.item_dividend_dividend)
            date = itemView.findViewById(R.id.item_dividend_date)

            itemView.setOnClickListener {
                listener!!.clicked(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.item_dividend, parent, false)
        return DividendCell(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val cell = holder as DividendCell
        val item = dividendList[position]

        cell.category.text = item.category
        cell.dividend.text = BaseUtil.addComma(item.dividend)
        cell.date.text = item.date
    }

    override fun getItemCount(): Int {
        return this.dividendList.size
    }

    fun setDividendList(items: ArrayList<Dividend>) {
        this.dividendList = items
        notifyDataSetChanged()
    }

    interface DividendAdapterListener {
        fun clicked(position: Int)
    }
}