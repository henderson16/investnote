package com.henderson.finance.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.henderson.finance.R

class TipAdapter : RecyclerView.Adapter<TipAdapter.ViewHolder>() {
    val bannerCount = 2

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tipMessage: TextView = itemView.findViewById(R.id.item_tip_msg)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val root = inflater.inflate(R.layout.item_tip, parent, false)
        return ViewHolder(root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (position) {
            0 -> holder.tipMessage.setText(R.string.tip_1)
            1 -> holder.tipMessage.setText(R.string.tip_2)
            else -> holder.tipMessage.text = null
        }
    }

    override fun getItemCount(): Int {
        return bannerCount
    }
}
