package com.henderson.finance.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_dividend")
class Dividend {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0

    @ColumnInfo(name = "category")
    var category: String? = null

    @ColumnInfo(name = "dividend")
    var dividend: Int = 0

    @ColumnInfo(name = "date")
    var date: String? = null
}