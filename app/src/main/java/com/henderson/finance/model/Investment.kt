package com.henderson.finance.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_investment")
class Investment {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0

    @ColumnInfo(name = "category")
    var category: String? = null

    @ColumnInfo(name = "price")
    var price: Int = 0

    @ColumnInfo(name = "collection")
    var collection: Int = 0

    @ColumnInfo(name = "fromDate")
    var fromDate: String? = null

    @ColumnInfo(name = "toDate")
    var toDate: String? = null

    //	@Ignore
    //	public boolean isFirst = true;
}