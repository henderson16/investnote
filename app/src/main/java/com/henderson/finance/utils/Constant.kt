package com.henderson.finance.utils

object Constant {
    const val FIRST_LAUNCH = "first_launch"
    const val AVAILABLE_COUNT = "available_count"

    const val NEW_COIN = 3
}
