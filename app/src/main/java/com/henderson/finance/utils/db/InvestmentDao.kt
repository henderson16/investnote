package com.henderson.finance.utils.db

import androidx.room.*
import com.henderson.finance.model.Investment

@Dao
interface InvestmentDao {
    @Query("SELECT * FROM tb_investment")
    fun getInvestmentList(): List<Investment>

    @Insert
    fun insertInvestment(investment: Investment): Long

    @Delete
    fun deleteInvestment(investment: Investment)

    @Update
    fun updateInvestment(investment: Investment)
}