package com.henderson.finance.utils

import android.content.Context
import android.content.SharedPreferences
import com.henderson.finance.BuildConfig

/**
 * 1. init object
 * 2. save data
 * 3. commit
 * OR
 * 1. get data using context
 */
class PreferenceManager(val context: Context) {
    var preferences: SharedPreferences
    var editor: SharedPreferences.Editor

    init {
        preferences = getPreferences(context)
        editor = preferences.edit()
    }

    fun saveString(key: String?, value: String?) {
        editor.putString(key, value)
    }

    fun saveBoolean(key: String?, value: Boolean) {
        editor.putBoolean(key, value)
    }

    fun saveInt(key: String?, value: Int) {
        editor.putInt(key, value)
    }

    fun commit() {
        editor.commit()
    }

    fun getAvailableCount(): Int {
        return getInt(context, Constant.AVAILABLE_COUNT)
    }

    fun addAvailableCount() {
        val savedCoin = getInt(context, Constant.AVAILABLE_COUNT)
        val preferenceManager = PreferenceManager(context)
        preferenceManager.saveInt(Constant.AVAILABLE_COUNT, savedCoin + Constant.NEW_COIN)
        preferenceManager.commit()
    }

    fun removeAvailableCount() {
        val preferenceManager = PreferenceManager(context)
        preferenceManager.saveInt(Constant.AVAILABLE_COUNT, getInt(context, Constant.AVAILABLE_COUNT)-1)
        preferenceManager.commit()
    }

    companion object {
        private const val NAME = BuildConfig.APPLICATION_ID
        private const val DEFAULT_STRING = ""
        private const val DEFAULT_BOOLEAN = false
        private const val DEFAULT_INT = -1

        private fun getPreferences(context: Context): SharedPreferences {
            return context.getSharedPreferences(NAME, Context.MODE_PRIVATE)
        }

        fun getString(context: Context, key: String?): String? {
            val prefs = getPreferences(context)
            return prefs.getString(key, DEFAULT_STRING)
        }

        fun getBoolean(context: Context, key: String?): Boolean {
            val prefs = getPreferences(context)
            return prefs.getBoolean(key, DEFAULT_BOOLEAN)
        }
        fun getInt(context: Context, key: String?): Int {
            val prefs = getPreferences(context)
            return prefs.getInt(key, DEFAULT_INT)
        }
    }
}