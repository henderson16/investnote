package com.henderson.finance.utils.db

import androidx.room.*
import com.henderson.finance.model.Dividend
import com.henderson.finance.model.Investment

@Dao
interface DividendDao {
    @Query("SELECT * FROM tb_dividend")
    fun getDividendList(): List<Dividend>

    @Insert
    fun insertDividend(dividend: Dividend): Long

    @Delete
    fun deleteDividend(dividend: Dividend)

    @Update
    fun updateDividend(dividend: Dividend)
}