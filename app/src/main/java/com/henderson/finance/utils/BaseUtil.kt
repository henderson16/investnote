package com.henderson.finance.utils

object BaseUtil {
    fun addComma(num: Int): String {
        if (num >= 0) {
            var number = num.toString()
            var i = number.length - 3
            while (i > 0) {
                number = number.substring(0, i) + "," + number.substring(i)
                i -= 3
            }
            return number
        } else {
            var number = (num * -1).toString()
            var i = number.length - 3
            while (i > 0) {
                number = number.substring(0, i) + "," + number.substring(i)
                i -= 3
            }
            //TODO change something
            return ("-" + number)
        }
    }
}